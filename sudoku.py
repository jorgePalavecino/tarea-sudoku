sudoku =[[5,3,0,0,7,0,0,0,0],
         [6,0,0,1,9,5,0,0,0],
         [0,9,8,0,0,0,0,6,0],
         [8,0,0,0,6,0,0,0,3],
         [4,0,0,8,0,3,0,0,1],
         [7,0,0,0,2,0,0,0,6],
         [0,6,0,0,0,0,2,8,0],
         [0,0,0,4,1,9,0,0,5],
         [0,0,0,0,8,0,0,7,9]] 

def matriz(x, y, sudoku): #funcion que define las cordenadas de cada subMatriz
    cordenadaX= cor(x)
    cordenadaY= cor(y)

    cuadro=[]
    for fila in sudoku[cordenadaY *3: cordenadaY *3+3]:
        for col in fila[cordenadaX *3: cordenadaX *3+3]:
            cuadro.append(col)
            
    return cuadro        

def cor(valor):     #funcion define el numero de subMatriz3x3 que pertenece 
    if valor <=2:
        return 0
    elif valor <=5:
        return 1
    else:
        return 2     

def print_ordenado(sudoku):  # funcion que permite ordenar la matriz
    for l in sudoku:
        print(l)

def distinto(x, y ,w, sudoku):  #funcion para confirmar valores distintos
    #fila 
    if w in sudoku[y]:
        return False
    #columna
    if w in [fila[x] for fila in sudoku]:
        return False
    #SubMatriz 
    matriz3x3 = matriz(x, y, sudoku)
    if w in matriz3x3:
        return False
    return True

def solucion(sudoku):
    for y in range(9):
        for x in range(9):
            if sudoku[y][x] == 0:
                for z in range(1,10):
                    if distinto(x, y, z, sudoku):
                        sudoku[y][x] = z
                        solucion(sudoku)          #recurcividad 
                        sudoku[y][x] = 0          #Backtrackig
                return
    print_ordenado(sudoku)            
    return    

solucion(sudoku)
